import { Component, OnInit } from '@angular/core';
import { ComisionGrupo } from './shared/interfaces/comision-grupo.interface';
import { ComisionService } from './shared/services/comision.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'comision-test';

  constructor(private comisionService: ComisionService) { }

  ngOnInit(): void {
    this.fetchData();
    this.delete(1);

    const cg: ComisionGrupo = {
      Id: 0,
      nombre: 'Private Employee',
      comisionesGenerales: [{ amount: 5000, payed: true }, { amount: 3000, payed: false }]
    }
    this.save(cg);

    const cgu: ComisionGrupo = {
      Id: 1,
      nombre: 'Public Employee',
      description: 'Empleados gubernamentales',
      comisionesGenerales: [{ amount: 5000, payed: true }, { amount: 3000, payed: false }]
    }
    this.update(cgu);
  }

  /**
   * @returns Retorna la lista de elementos ComisionGrupo
   */
  fetchData() {
    this.comisionService.findAll().subscribe(items => {
      console.log(`FindAll: ${items}`)
    }, error => console.log(`FINDALL: ${error.url}`));
  }

  /**
   * Registra un objeto ComisionGrupo
   * @param cg ComisionGrupo
   * @returns el nuevo elemento ComisionGrupo
   */
  save(cg: ComisionGrupo) {
    this.comisionService.save(cg).subscribe(x => {
      console.log(`Save: ${x}`)
    }, error => console.log(`SAVE: ${error.url}`));
  }

  /**
    * edita un objeto ComisionGrupo
    * @param cg ComisionGrupo
    * @returns el elemento ComisionGrupo editado
    */
  update(cg: ComisionGrupo) {
    this.comisionService.update(cg.Id, cg).subscribe(x => {
      console.log(`Update: ${x}`)
    }, error => console.log(`UPDATE: ${error.url}`));
  }

  /**
   * Remueve un objeto ComisionGrupo
   * @param id - Objeto ComisionGrupo
   * @returns true si el elemento fue borrado
   */
  delete(id: number) {
    this.comisionService.delete(id).subscribe(x => {
      console.log(`Delete: ${x}`)
    }, error => console.log(`DELETE: ${error.url}`));
  }
}
