import { ComisionGeneral } from "./comision-general.interface";
import { IEntidadBaseConId } from "./i-entidad-base-con-id.interface";

export interface ComisionGrupo extends IEntidadBaseConId {
  comisionesGenerales: ComisionGeneral[];
  nombre: string;
  description?: string;
}