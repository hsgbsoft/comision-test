import { Observable } from 'rxjs';

export interface BaseCrudService<T, IEntidadBaseConId> {
  save(t: T): Observable<T>;
  update(id: IEntidadBaseConId, t: T): Observable<T>;
  findAll(): Observable<T>;
  delete(id: IEntidadBaseConId): Observable<any>;
  httpRoute(): string;
}

