import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ComisionGrupo } from '../interfaces/comision-grupo.interface';
import { CrudService } from './crud.service';
import { environment } from '../environment/environment';
const HTTP_OPTIONS = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class ComisionService extends CrudService<ComisionGrupo, number>  {

  constructor(protected override _http: HttpClient) {
    super(_http, `${environment.api.baseUrl}/comisiongrupo`);
  }
}

