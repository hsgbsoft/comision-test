import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { BaseCrudService } from './base-crud.service';
const HTTP_OPTIONS = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

export abstract class CrudService<T, ID> implements BaseCrudService<T, ID> {

  constructor(
    protected _http: HttpClient,
    protected _base: string
  ) { }

  httpRoute(): string {
    return this._base;
  }

  save(t: T): Observable<T> {
    return this._http.post<T>(this.httpRoute(), t, HTTP_OPTIONS);
  }

  update(id: ID, t: T): Observable<T> {
    return this._http.put<T>(`${this.httpRoute()}/${id}`, t, HTTP_OPTIONS);
  }

  findAll(): Observable<T> {
    return this._http.get<T>(this.httpRoute());
  }

  delete(id: ID): Observable<T> {
    return this._http.delete<T>(`${this.httpRoute()}/${id}`);
  }
}

